from tkinter import *
import tkinter.simpledialog
import OpenCVCanvas
import threading
import cv2


class StreamingLaneDetector(Tk):

	def __init__(self):
		# Call super constructor
		Tk.__init__(self)
		
		# put the window at the top left of the screen
		self.geometry("+0+0")

		# Get the current screen dimensions...
		screen_width = self.winfo_screenwidth()
		screen_height = self.winfo_screenheight()
		# ... and size the display windows appropriately
		windowWidth = screen_width / 2
		windowHeight = (screen_height-200)/ 2
		
		# Build the menu bars
		menu1 = Frame(self)
		menu1.pack()
		
		Button(menu1, text="Load", command=self.get_camIP).pack(side=LEFT)
		Button(menu1, text="Quit", command= self.quit).pack(side=LEFT)

		# Display video(s) row
		videoRow1 = Frame(self)
		videoRow1.pack()

		# Video screens
		self.videoCanvas1 = OpenCVCanvas.OpenCVCanvas(videoRow1, height=windowHeight, width=windowWidth)
		self.videoCanvas1.pack(side=LEFT)
		
		self.cam_ip = "http://mobotix/cgi-bin/image.jpg"

		
	def get_camIP(self):
        
		ipString = tkinter.simpledialog.askstring("IP Cam Address", "IP Address:", initialvalue=self.cam_ip)
        
		# Run the IP cam viewer in a background thread
		t = threading.Thread(target=self.videoCanvas1.listen, args=(ipString, lambda x: cv2.Canny(x, 100, 200)))
		# because this is a daemon, it will die when the main window dies
		t.setDaemon(True)
		t.start()

app = StreamingLaneDetector()
app.mainloop()
