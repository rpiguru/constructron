from tkinter import *
from tkinter import filedialog
import cv2
import PIL
from PIL import Image, ImageTk
import numpy as np
import math

class LaneDetector(Tk):
	
	def __init__(self):
		Tk.__init__(self)
		self.geometry("+0+0")
		menu = Frame(self)
		menu.pack()
		
		screen_width = self.winfo_screenwidth()
		screen_height = self.winfo_screenheight()
		windowWidth = screen_width / 3
		windowHeight = (screen_height-200)/ 2
		
		# Constants
		self.maxSaturation = 255
		self.maxValue = 255
		self.hueCenter = 31		# safety orange:  https://en.wikipedia.org/wiki/Safety_orange
		
		# Show the images
		controlRow1 = Frame(self)
		controlRow1.pack()
		controlRow2 = Frame(self)
		controlRow2.pack()
		imagesRow1 = Frame(self)
		imagesRow1.pack()
		imagesRow2 = Frame(self)
		imagesRow2.pack()
		
		self.imageCanvas1 = Canvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas1.pack(side=LEFT)
		self.imageCanvas1.img = None
		self.imageCanvas2 = Canvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas2.pack(side=LEFT)
		self.imageCanvas2.img = None
		self.imageCanvas3 = Canvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas3.pack(side=LEFT)
		self.imageCanvas3.img = None
		self.imageCanvas4 = Canvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas4.pack(side=LEFT)
		self.imageCanvas4.img = None
		self.imageCanvas5 = Canvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas5.pack(side=LEFT)
		self.imageCanvas5.img = None
		self.imageCanvas6 = Canvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas6.pack(side=LEFT)
		self.imageCanvas6.img = None
		
		self.rawImage = None
		
		self.pyramids = StringVar()
		self.pyramids.set(1)
		Label(controlRow1, text = "Pyrs").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=5, increment=1, textvariable=self.pyramids, command=lambda: self.pyrDown(self.rawImage)).pack(side=LEFT)
		
		# Tolerance = distance above AND below (in degrees, same as hueCenter)
		self.hueTolerance = StringVar()
		self.hueTolerance.set(8)
		Label(controlRow1, text = "Hue Tol").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=15, increment=1, textvariable=self.hueTolerance, command=lambda: self.FilterColor(self.imageCanvas1.img)).pack(side=LEFT)

		self.gaussianSigma = StringVar()
		self.gaussianSigma.set(1)
		Label(controlRow1, text = "Pre-Filter Sgm").pack(side=LEFT)
		Spinbox(controlRow1, from_=1, to=11, increment=2, textvariable=self.gaussianSigma, command=lambda: self.FilterColor(self.imageCanvas1.img)).pack(side=LEFT)

		self.minSaturation = StringVar()
		self.minSaturation.set(40)
		Label(controlRow1, text = "Min sat").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=100, increment=5, textvariable=self.minSaturation, command=lambda: self.FilterColor(self.imageCanvas1.img)).pack(side=LEFT)

		self.minValue = StringVar()
		self.minValue.set(140)
		Label(controlRow1, text = "Min val").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=255, increment=5, textvariable=self.minValue, command=lambda: self.FilterColor(self.imageCanvas1.img)).pack(side=LEFT)

		self.erodeSize = StringVar()
		self.erodeSize.set(4)
		Label(controlRow1, text = "Erode").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=40, increment=2, textvariable=self.erodeSize, command=lambda: self.morph(self.imageCanvas2.img)).pack(side=LEFT)

		self.dilateSize = StringVar()
		self.dilateSize.set(6)
		Label(controlRow2, text = "Dilate").pack(side=LEFT)
		Spinbox(controlRow2, from_=0, to=40, increment=2, textvariable=self.dilateSize, command=lambda: self.morph(self.imageCanvas2.img)).pack(side=LEFT)
		
		self.morphSigma = StringVar()
		self.morphSigma.set(1)
		Label(controlRow2, text = "Post-Morph Sgm").pack(side=LEFT)
		Spinbox(controlRow2, from_=1, to=11, increment=2, textvariable=self.morphSigma, command=lambda: self.morph(self.imageCanvas2.img)).pack(side=LEFT)

		self.cannyThreshLow = StringVar()
		self.cannyThreshLow.set(60)
		Label(controlRow2, text = "Canny Lo").pack(side=LEFT)
		Spinbox(controlRow2, from_=0, to=200, increment=5, textvariable=self.cannyThreshLow, command=lambda: self.canny(self.imageCanvas3.img)).pack(side=LEFT)

		self.cannyThreshHigh = StringVar()
		self.cannyThreshHigh.set(180)
		Label(controlRow2, text = "Canny Hi").pack(side=LEFT)
		Spinbox(controlRow2, from_=50, to=500, increment=5, textvariable=self.cannyThreshHigh, command=lambda: self.canny(self.imageCanvas3.img)).pack(side=LEFT)
		
		self.minContourSize = StringVar()
		self.minContourSize.set(500)
		Label(controlRow2, text = "Min Contour").pack(side=LEFT)
		Spinbox(controlRow2, from_=50, to=2000, increment=50, textvariable=self.minContourSize, command=lambda: self.contourPoly(self.imageCanvas4.img)).pack(side=LEFT)
				
		self.minAspectRatio = StringVar()
		self.minAspectRatio.set(.8)
		Label(controlRow2, text = "Min Aspect Ratio").pack(side=LEFT)
		Spinbox(controlRow2, from_=.2, to=2, increment=.1, textvariable=self.minAspectRatio, command=lambda: self.contourPoly(self.imageCanvas4.img)).pack(side=LEFT)
				
		Button(menu, text='Load Image', command=lambda: self.loadImage(self.imageCanvas1)).pack(side=LEFT)
		
	# query user and open image
	def loadImage(self, canvas):
		
		openImage = filedialog.askopenfilename()
		
		if openImage:
			
			# OpenCV reads in BGR - switch to RGb
			image = cv2.cvtColor(cv2.imread(openImage), cv2.COLOR_BGR2RGB)
			image = cv2.pyrDown(image)
			canvas.img = image

			if canvas.img is not None:
				self.rawImage = canvas.img
				self.pyrDown(self.rawImage)		
				
	# Display numpy array as image on GUI			
	def publishArray(self, numpyArray, canvas):
		
			canvas.img = numpyArray
		
			# Scale to fit window size
			if numpyArray.shape[0] > canvas.winfo_height() or numpyArray.shape[1] > canvas.winfo_width():
				
				# rows/cols  , y/x
				arrayRatio = float(numpyArray.shape[0])/numpyArray.shape[1]						
				canvasRatio = float(canvas.winfo_height() / canvas.winfo_width())
				
				# array height controls
				if arrayRatio > canvasRatio:
					shrinkRatio = float(canvas.winfo_height()) / numpyArray.shape[0]
				# array width controls
				else:
					shrinkRatio = float(canvas.winfo_width()) / numpyArray.shape[1]
								
				numpyArray = cv2.resize(numpyArray, (0,0), fx=shrinkRatio, fy=shrinkRatio)
				
			# Turn the numpy array into an image
			image = Image.fromarray(numpyArray.astype(np.uint8))

			# display on the canvas
			canvas.imagePhoto = ImageTk.PhotoImage(image)
			canvas.create_image((numpyArray.shape[1]/2,numpyArray.shape[0]/2),image=canvas.imagePhoto)
			
	def pyrDown(self, image):
		if self.rawImage is None:
			return
		
		for i in range(int(self.pyramids.get())):
			image = cv2.pyrDown(image)
		self.publishArray(image, self.imageCanvas1)
		self.FilterColor(image)
			
	def FilterColor(self, image):
		if self.rawImage is None:
			return
			
		# Smooth out the image 
		blurred = cv2.GaussianBlur(image,(0,0), int(self.gaussianSigma.get()))
		
		# Convert image to HSV (Hue, Saturation, Value) format
		hsvImage = cv2.cvtColor(blurred, cv2.COLOR_RGB2HSV_FULL)
		
		# With HSV_FULL, the 0-360 degree hue range is mapped to the 0-255 space of uint8
		# We need to account for that 		
		HSVlowerBound = np.array([self.hueCenter/360.0*255-float(self.hueTolerance.get())/360.0*255, int(self.minSaturation.get()), int(self.minValue.get())], np.uint8)
		HSVupperBound = np.array([self.hueCenter/360.0*255+float(self.hueTolerance.get())/360.0*255, self.maxSaturation, self.maxValue], np.uint8)		
		orange = cv2.inRange(hsvImage, HSVlowerBound, HSVupperBound)
		
		self.publishArray(orange, self.imageCanvas2)	
		
		self.morph(orange)			
		
	def morph(self, image):
		if self.rawImage is None:
			return
		
		secondSigma = float(self.morphSigma.get())
		
		# morph
		erodeSize = int(self.erodeSize.get())
		if erodeSize > 0:
			erodeElement = cv2.getStructuringElement(cv2.MORPH_RECT, (erodeSize,erodeSize))
			eroded = cv2.erode(image, erodeElement) 
		else:
			eroded = image

		dilateSize = int(self.dilateSize.get())
		if dilateSize > 0:
			dilateElement = cv2.getStructuringElement(cv2.MORPH_RECT, (dilateSize,dilateSize))
			dilated = cv2.dilate(eroded, dilateElement)
		else:
			dilated = eroded
		
		blurred = cv2.GaussianBlur(dilated,(0,0), secondSigma)
		
		self.publishArray(blurred, self.imageCanvas3)

		self.canny(blurred)
		
	def canny(self, image):
		if self.rawImage is None:
			return

		canny = cv2.Canny(image, float(self.cannyThreshLow.get()), float(self.cannyThreshHigh.get()))
		self.publishArray(canny, self.imageCanvas4)
		self.contourPoly(canny)
		
	def contourPoly(self, image):
		if self.rawImage is None:
			return
		
		im2, contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)		
		contourShow = np.zeros(image.shape, image.dtype)
		approxContour = []
		for contour in contours:
			if  cv2.contourArea(contour) > int(self.minContourSize.get()):
				# turn the contour into a polygon
				polyContour = cv2.approxPolyDP(contour, 8, True)
				# kill concavities
				convexContour = cv2.convexHull(polyContour)
				# cones are simple
				if len(convexContour) >= 3 and len(convexContour) <= 10 and self.checkAspectRatio(convexContour):
					approxContour.append(convexContour)
				
			
		cv2.drawContours(contourShow, approxContour, -1, (255,0,0))
			
		self.publishArray(contourShow, self.imageCanvas5)
		
		self.drawCones(approxContour)
		
	def drawCones(self, contours):
		result = self.imageCanvas1.img.copy()
		
		for contour in contours:
			contourNumpy = np.array(contour)
			left = min(contourNumpy[:,:,0])
			right = max(contourNumpy[:,:,0])
			top = min(contourNumpy[:,:,1])
			bottom = max(contourNumpy[:,:,1])
			cv2.rectangle(result, (left, top), (right, bottom), (0,255,0), 3)
		
		self.publishArray(result, self.imageCanvas6)
		
	def checkAspectRatio(self, contour):
		contourNumpy = np.array(contour)
		left = min(contourNumpy[:,:,0])
		right = max(contourNumpy[:,:,0])
		top = min(contourNumpy[:,:,1])
		bottom = max(contourNumpy[:,:,1])
		# check aspect ratio
		return float(right-left)/(bottom-top) < float(self.minAspectRatio.get())
			
	def isPointingUp(self, contour):
		contourNumpy = np.array(contour)
		left = min(contourNumpy[:,:,0])
		right = max(contourNumpy[:,:,0])
		top = min(contourNumpy[:,:,1])
		bottom = max(contourNumpy[:,:,1])
		# find halfways point
		halfUp = (bottom+top) / 2
		# make sure the base is low
		leftPos = np.argmin(contourNumpy[:,:,0])
		rightPos = np.argmax(contourNumpy[:,:,0])
		if contourNumpy[leftPos][0][1] < halfUp or contourNumpy[rightPos][0][1] < halfUp :
			return False
		return True

app = LaneDetector()
app.mainloop()
