from tkinter import *
from tkinter import filedialog
import cv2
import PIL
from PIL import Image, ImageTk
import numpy as np
import math
from OpenCVCanvas import OpenCVCanvas
from ClickableOpenCVCanvas import ClickableOpenCVCanvas
import LaneDetectionAlg

class LaneDetectorGUI(Tk):
	
	def __init__(self):
		Tk.__init__(self)
		self.geometry("+0+0")
		
		screen_width = self.winfo_screenwidth()
		screen_height = self.winfo_screenheight()
		windowWidth = screen_width / 3
		windowHeight = (screen_height-200)/ 2
		
		# Constants
		self.maxSaturation = 255
		self.maxValue = 255
		self.hueCenter = 31		# safety orange:  https://en.wikipedia.org/wiki/Safety_orange
		
		# Show the images
		controlRow1 = Frame(self)
		controlRow1.pack()
		controlRow2 = Frame(self)
		controlRow2.pack()
		imagesRow1 = Frame(self)
		imagesRow1.pack()
		imagesRow2 = Frame(self)
		imagesRow2.pack()
		
		self.imageCanvas1 = ClickableOpenCVCanvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas1.pack(side=LEFT)
		self.imageCanvas1.img = None
		self.imageCanvas2 = OpenCVCanvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas2.pack(side=LEFT)
		self.imageCanvas2.img = None
		self.imageCanvas3 = OpenCVCanvas(imagesRow1, height=windowHeight, width=windowWidth)
		self.imageCanvas3.pack(side=LEFT)
		self.imageCanvas3.img = None
		self.imageCanvas4 = OpenCVCanvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas4.pack(side=LEFT)
		self.imageCanvas4.img = None
		self.imageCanvas5 = OpenCVCanvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas5.pack(side=LEFT)
		self.imageCanvas5.img = None
		self.imageCanvas6 = OpenCVCanvas(imagesRow2, height=windowHeight, width=windowWidth)
		self.imageCanvas6.pack(side=LEFT)
		self.imageCanvas6.img = None
		
		self.selectedPoint = None
		self.imageCanvas1.setCallbacks(self.pickPoint, lambda: self.pickPoint(None)) 
		
		self.rawImage = None
		self.pyrImage = None
		self.blurredImage = None
		self.segImage = None
		
		Button(controlRow1, text='Load Image', command=self.loadImage).pack(side=LEFT)	
		
		self.pyramids = StringVar()
		self.pyramids.set(0)
		Label(controlRow1, text = "Pyrs").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=5, increment=1, textvariable=self.pyramids, command=self.pyrDown).pack(side=LEFT)
		
		self.gaussianSigma = StringVar()
		self.gaussianSigma.set(1)
		Label(controlRow1, text = "Pre-Segment Sgm").pack(side=LEFT)
		Spinbox(controlRow1, from_=1, to=11, increment=2, textvariable=self.gaussianSigma, command=self.PreFilter).pack(side=LEFT)

		self.segmentBins = StringVar()
		self.segmentBins.set(8)
		Label(controlRow1, text = "Color Bins").pack(side=LEFT)
		Spinbox(controlRow1, from_=0, to=20, increment=1, textvariable=self.segmentBins, command=self.segmentColor).pack(side=LEFT)
	
	# We can reuse this for clear as well, since the clear callback is only called after clickPoint is set to None
	def pickPoint(self, point):
		self.selectedPoint = point
		self.colorMask()
		
	def loadImage(self):
		self.imageCanvas1.loadImage()
		self.rawImage = self.imageCanvas1.img		
		self.pyrDown()
		
	def pyrDown(self):
		if self.rawImage is None:
			return
			
		self.pyrImage = self.rawImage		
		
		for i in range(int(self.pyramids.get())):
			self.pyrImage = cv2.pyrDown(self.pyrImage)
		self.imageCanvas1.publishArray(self.pyrImage)
		self.PreFilter()
		
	def PreFilter(self):
		if self.pyrImage is None:
			return
				
		self.blurredImage = cv2.GaussianBlur(self.pyrImage,(0,0), int(self.gaussianSigma.get()))
		
		self.imageCanvas2.publishArray(self.blurredImage)
		self.segmentColor()

	def segmentColor(self):		
		if self.blurredImage is None:
			return
		self.segImage = LaneDetectionAlg.ColorSegment(self.blurredImage, int(self.segmentBins.get()))
		self.imageCanvas3.publishArray(self.segImage)
		
	def colorMask(self):
		if self.segImage is None or self.selectedPoint is None:
			return
		# This needs to copy scaling from an image too large for the OpenCVCanvas	
		print (self.selectedPoint.x, self.selectedPoint.y)
		color = self.segImage[self.selectedPoint.y, self.selectedPoint.x]
		print(color)
		self.maskImage = LaneDetectionAlg.segMask(self.segImage, color)
		self.imageCanvas4.publishArray(self.maskImage)


app = LaneDetectorGUI()
app.mainloop()
