#! /usr/bin/python
if __name__ == '__main__':
    from EXbee import EXbee

    kk = EXbee('COM6', 9600)

    response = kk.send_tx("Hi, This is End Device", "0013A20040DB7BA6", "02")

    print(response)

    if response['Delivery_status'][:2] == "00":
        print("Frame sent with success")
    else:
        print("Failed")

