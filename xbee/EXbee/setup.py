from distutils.core import setup

setup(
    name="EXbee",
    version='0.0.1',
    py_modules=['EXbee'],
    author='Wester de Weerdt',
    author_email='wester.de.weerdt@dutchmail.com',
    url='https://github.com/openedhardware/EXbee',
    description='Python wrapper for XBee pro 3B Programmable',
    license='MIT'
)
