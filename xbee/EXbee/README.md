============
Xbee Zigbee
============

EXbee provides an implementation of the Serial Communication API for XBee Pro 3B Programmable Module(XBP9B-DMSTB002), this 
is the first version v1.0 , it support the frame types (Zigbee) : 

    1- 0x10  Transmit Request
    2- 0x8b  Transmit Status
    3- 0x90  Receive Packet
    4- 0x08  AT Command
    5- 0x88  AA Command Response
    6- 0x17  Remote AT
    7- 0x97  Remote AT Command Response

It allows one to easily access advanced features of one or more XBee devices from an application written in Python. 

Before using this library, please refer the *README* of this repo to set XBee modules correctly.

(This library works with API mode with Escapes of XBee.)

An example use case might look like this::

    ```python
    #! /usr/bin/python
    import EXbee 
    kk = EXbee.EXbee('/dev/ttyUSB0',9600)
    
    # When XBee module is plugged in, we need to initialize it.
    # Once it is initialized, no need to initialize again until it is unplugged.
    kk.initialize()
    
    # To get Device ID, call the function below.
    print kk.get_device_id()

    # To execute AT command:
    kk.execute_at('PL', '03', is_ascii=False)
    
    # To execute remote AT command:
    kk.send_remote_at("SL", "0013A20040DB7BA6")
    
    # To send data packet to the remote node:
    response = kk.send_tx("Hi, This is End Device", "0013A20040DB7BA6", "02")
    if response['Delivery_status'][:2] == "00":
        print "Frame sent with success"
    else:
        print "Failed"
    
    # To receive data from the remote node:
    response = kk.read_rx()
    print "Sender:%s     " % response['SOURCE_ADDRESS_64'] 
    print "Message: %s   " % response['DATA']
    
    ```  

Dependencies
============
    
    PySerial
