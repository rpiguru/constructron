# Copyright ConstrucTRON Inc, 2016

import cv2
import numpy as np
import VehicleMOG
import sys
import time

# command line arguments
showProcessFlag = False	    # display intermeadiate processing
goSlow = False	            # slow down the display (works better with video files)
processOn = True            # turn to false to simply check out the video stream

if len(sys.argv) > 1:
    showProcessFlag = True
    if sys.argv[1] == 'slow':
        goSlow = True
        print ("Going Slow")
    if sys.argv[1] == 'off':
        processOn = False
        print ("Processing off")

# locate the ip cam        
cam_ip = "http://192.168.1.3/snap.jpg"
        
# build the subwindows        
top = 100
bottom = 230
left = 100
middle = 170
right = 240

left_count = 0
right_count = 0

left_window_color = (0,255,255)
right_window_color = (255,0,255)

left_window = VehicleMOG.WindowBox(top, bottom, left, middle, left_window_color)
right_window = VehicleMOG.WindowBox(top, bottom, middle, right, right_window_color)

# loop until interrupted
while(True):

    cap = cv2.VideoCapture(cam_ip)

    if cap.isOpened():
        ret,img = cap.read()
        
        if processOn:

            img, entrants = VehicleMOG.doMOG(img, [left_window, right_window], showProcessFlag)
            
            left_count += entrants[0]
            right_count += entrants[1]
                           
            # scoreboard
            cv2.putText(img, str(left_count), (int((middle+left)/2)-40, int((bottom+top)/2)+25), cv2.FONT_HERSHEY_SIMPLEX, 2, left_window_color, 10)
            cv2.putText(img, str(right_count), (int((middle+right)/2)-40, int((bottom+top)/2)+25), cv2.FONT_HERSHEY_SIMPLEX, 2, right_window_color, 10)
        
        cv2.imshow("win",img)
		
    key = cv2.waitKey(1)
    # quit
    if key & 0xFF == ord('q'):  
        break
    # clear the score
    if key & 0xFF == ord('r'):
        left_count = 0
        right_count = 0
        
    if goSlow:
        time.sleep(.1)
