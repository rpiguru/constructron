from tkinter import *
import cv2
import time

# DESKTOP:: Comment these lines to run on the desktop
#from picamera.array import PiRGBArray
#from picamera import PiCamera

class IPCam:
        
    def __init__(self, defaultIP):
        self.IP = defaultIP
    
    def __str__(self):
        return "IP Cam"
        
    def showModeGUI(self, frame):
        Label(frame, text="IP Cam Address:").pack()

        self.ipStringVar = StringVar()
        self.ipStringVar.set(self.IP)
        self.ipEntry = Entry(frame, width=40, textvariable=self.ipStringVar)
        self.ipEntry.pack(expand=1)

        self.ipEntry.selection_range(0, END)
        self.ipEntry.focus_set()
        
        
    def getFrame(self):
                
        cap = cv2.VideoCapture(self.IP)
        
        if cap.isOpened():
            ret,img = cap.read()
            
            if img != []:                
                return img
                
        else:
            return None

        

class PiCam:
    
    def __init__(self):
        
        # DESKTOP:: Unomment this lines to run on the desktop
        self.camera = None
        
        # DESKTOP:: Comment these lines to run on the desktop
        self.camera = PiCamera()
        self.camera.resolution = (320, 240)
        self.rawCapture = PiRGBArray(self.camera)
        
    
    def __str__(self):
        return "Pi Cam"
        
    def showModeGUI(self, frame):
        Label(frame, text="Using PiCam").pack()
        
    def getFrame(self):
        
        # DESKTOP:: Uncomment these lines to run on the desktop
        # return None
        
        # DESKTOP:: Comment these lines to run on the desktop
        # grab an image from the camera
        time.sleep(0.1)
        self.rawCapture.truncate(0)
        self.camera.capture(self.rawCapture, format="bgr")
        return self.rawCapture.array
        
        
class USBCam:
    
    def __init__(self):
        self.vc = cv2.VideoCapture(0)
    
    def __str__(self):
        return "USB Cam"

    def showModeGUI(self, frame):
        Label(frame, text="Using USBCam").pack()

    def getFrame(self):
        if self.vc.isOpened(): # try to get the first frame
            rval, frame = self.vc.read()
        else:
            frame = None            
        return frame
