# Copyright ConstrucTRON Inc, 2016

from tkinter import *
import tkinter.simpledialog
import cv2
import numpy as np
import IPcam
import threading
from VehicleMOG2 import VehicleMOG
from VideoCaptureModes import *
from ClickableOpenCVCanvas import ClickableOpenCVCanvas
    
defaultCamIP = "http://192.168.1.15/snap.jpg"

class VideoSourceDialog():
        
    def __init__(self, parentPos, dialogSize, videoCaptureModes, camIndex, setCam):

        self.top = Toplevel()
        self.top.title("Video Source")
        self.top.geometry(str(dialogSize[0])+"x"+str(dialogSize[1])+"+"+str(parentPos[0]+50)+"+"+str(parentPos[1]+30))
        
        modeBar = Frame(self.top)
        modeBar.pack()

        self.videoCaptureModes = videoCaptureModes
        self.currentVideoMode = camIndex

        videoModeValue = StringVar()
        videoModeValue.set(str(self.videoCaptureModes[self.currentVideoMode]))        

        for mode in self.videoCaptureModes:
            Radiobutton(modeBar, indicatoron=0, text=str(mode), variable=videoModeValue, value=str(mode), command=lambda: self.changeMode(videoModeValue.get(), modeDetails)).pack(side=LEFT)
        
        modeDetails = Frame(self.top)
        modeDetails.pack()

        self.videoCaptureModes[self.currentVideoMode].showModeGUI(modeDetails)
        self.setCam = setCam
        
        buttonBar = Frame(self.top)
        buttonBar.pack()
        
        Button(buttonBar, text='Submit', command=self.send).pack(side=LEFT)
        Button(buttonBar, text='Cancel', command=self.top.destroy).pack(side=LEFT)        

    def changeMode(self, mode, modeDetails):
        modeNum = None
        for index in range(len(self.videoCaptureModes)):
            if mode == str(self.videoCaptureModes[index]):
                modeNum = index
                break        
        
        if(modeNum is not None):
            self.currentVideoMode = modeNum
            for widget in modeDetails.winfo_children():
                widget.destroy()
            self.videoCaptureModes[modeNum].showModeGUI(modeDetails)
        else:
            print("Error: Mode String Not Found")

    def send(self):        
        self.setCam(self.currentVideoMode)
        self.top.destroy()
        
class ApproachReaderGUI(Tk):
    
    videoCaptureModes = [IPCam(defaultCamIP), PiCam(), USBCam()]

    def __init__(self):
        Tk.__init__(self)
        self.geometry("+30+30")
        
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        windowWidth = screen_width / 3
        windowHeight = (screen_height-200)/ 2              
        
        buttonBar = Frame(self)
        buttonBar.pack()

        MODES = ["Raw","Detect","Debug"]

        self.modeValue = StringVar()
        self.modeValue.set(MODES[0])        

        for title in MODES:
            Radiobutton(buttonBar, indicatoron=0, text=title, variable=self.modeValue, value=title, command=self.setMode).pack(side=LEFT)
        
        Button(buttonBar, text="Load", command=self.getCam).pack(side=LEFT)
        Button(buttonBar, text="Pause", command=self.stopListening).pack(side=LEFT)
        Button(buttonBar, text="Reset", command=self.resetCount).pack(side=LEFT)
        Button(buttonBar, text="Quit", command= self.quit).pack(side=LEFT)
        
        # Setup the debug choices bar
        self.debugBar = Frame(self)
        self.debugBar.pack()
        # hide it, at first
        self.debugBar.pack_forget()
        
        # Which debug output should we show?
        DEBUG_MODES = ["MOG", "Eroded","Dilated","Thresholded", "Normal"]        
        self.debugValue = StringVar()
        self.debugValue.set(DEBUG_MODES[4])        

        for title in DEBUG_MODES:
            Radiobutton(self.debugBar, indicatoron=0, text=title, variable=self.debugValue, value=title).pack(side=LEFT)

        # Add the video window
        self.videoDisplay = ClickableOpenCVCanvas(self, height=windowHeight, width=windowWidth)
        self.videoDisplay.pack() 
                
        #  parameters:
        parameterBar = Frame(self)
        parameterBar.pack()
        
        # Erode element size
        erodeSize = IntVar()
        erodeSize.set(4)
        Scale(parameterBar, label='Erode', to_=8, variable=erodeSize, orient=HORIZONTAL, resolution=2).pack(side=LEFT)
        
        # Dilate element size
        dilateSize = IntVar()
        dilateSize.set(4)
        Scale(parameterBar, label='Dilate', to_=8, variable=dilateSize, orient=HORIZONTAL, resolution=2).pack(side=LEFT)    
            
        # Thresholding value
        threshSize = IntVar()
        threshSize.set(20)
        Scale(parameterBar, label='Threshold', to_=50, variable=threshSize, orient=HORIZONTAL, resolution=5).pack(side=LEFT)    
         
        # Detection settings 
        # Miniumum contor size (to be a vehicle)
        contourSize = IntVar()
        contourSize.set(100)
        Scale(parameterBar, label='Contour', to_=200, variable=contourSize, orient=HORIZONTAL, resolution=25).pack(side=LEFT)    
                
        # Top threshold height ("band"/starting line - when a vehichle crosses the line, it "enters" the box)
        enterSize = IntVar()
        enterSize.set(40)
        Scale(parameterBar, label='Entry width', to_=100, from_=10, variable=enterSize, orient=HORIZONTAL, resolution=10).pack(side=LEFT)    
         
        # Minimum vehicle distance for a "new" vehicle entry (sometimes a single vehicle is seen twice within the starting band in sucessive (or near-sucessive) frames)
        newVehicleDist = IntVar()
        newVehicleDist.set(20)
        Scale(parameterBar, label='New Vehicle Dist', to_=60, variable=newVehicleDist, orient=HORIZONTAL, resolution=5).pack(side=LEFT)    
        
        self.camIndex = 0
        
        self.MOG = VehicleMOG()
        
    def getCam(self):    
        vsd = VideoSourceDialog((self.winfo_x(), self.winfo_y()), (500,200), self.videoCaptureModes, self.camIndex, self.setCam)        
        
    def setCam(self, camIndex):
        self.camIndex = camIndex
        self.startVideo()
        
    def startVideo(self):    
        
        print("Using "+str(self.videoCaptureModes[self.camIndex]))        
        
        # Run the Get-Process-Show loop in a background thread
        t = threading.Thread(target=self.run)
        # because this is a daemon, it will die when the main window dies
        t.setDaemon(True)
        t.start()
        
    def run(self):
        self.listening = True        
        # loop until interrupted
        while(self.listening):
            currentFrame = self.getFrame()
            if currentFrame is not None:
                processedFrame = self.processFrame(currentFrame)
                if processedFrame is not None:
                    self.videoDisplay.publishArray(processedFrame, self.modeValue.get() == 'Detect')
                else:
                    self.videoDisplay.publishArray(currentFrame, False)
            else:
                msg = "Open "+str(self.videoCaptureModes[self.camIndex])+" failed."
                print("Warning!", msg)
                tkinter.messagebox.showwarning("Warning!", msg)
                self.listening = False

    def getFrame(self):
        return self.videoCaptureModes[self.camIndex].getFrame()
        
    def processFrame(self, currentFrame):
        
        # "Raw" mode means dump the raw IP Cam frame - no processing
        if self.modeValue.get() != "Raw":
            processedFrame, entrants = self.MOG.doMOG(currentFrame, self.videoDisplay.getWindowBoxes(), self.modeValue.get(), self.debugValue.get())      
            
            if self.modeValue.get() == 'Detect': 
                for windowPosition in range(len(self.videoDisplay.getWindowBoxes())):
                    self.videoDisplay.getWindowBoxes()[windowPosition].addCount(entrants[windowPosition])
            
            return processedFrame

        return None
        
    def setMode(self):
        mode = self.modeValue.get()
        print("Switching to ", self.modeValue.get(), " mode")
        if self.modeValue.get() == 'Debug':
            self.debugBar.pack()
        else:
            self.debugBar.pack_forget()           
        
    def stopListening(self):
        print("Stop listening")
        self.listening = False        
        
    def resetCount(self):
        for windowPosition in range(len(self.videoDisplay.getWindowBoxes())):
            self.videoDisplay.getWindowBoxes()[windowPosition].setCount(0)

app = ApproachReaderGUI()
app.title("Approach Reader")
app.mainloop()
            
