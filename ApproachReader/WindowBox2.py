# Copyright ConstrucTRON Inc, 2016

import cv2

# An subwindow/ROI
class WindowBox:

	# careful - t < b & l < r, or getSubImg will return an empty array
	def __init__(self, t, b, l, r, color):
		self.top = t
		self.bottom = b
		self.left = l
		self.right = r
		self.vehicles_old_frames = []        
		self.color = color
		self.count = 0
		
		self.CVcolors = {'cyan':(0,255,255), 'magenta':(255,0,255), 'yellow':(255,255,0)} 


	def getSubimg(self, img):
		return img[self.top:self.bottom,self.left:self.right]

	def publishToWindow(self, img, scene):
		img[self.top:self.bottom,self.left:self.right] = scene
	
	def drawCount(self, img):
		cv2.putText(img, str(self.count), (int(self.left)+10, int(self.bottom)-10), cv2.FONT_HERSHEY_SIMPLEX, 1, self.CVcolors[self.color], 3)    
		
	def clickInside(self, x, y):
		return y > self.top and y < self.bottom and x < self.right and x > self.left
		
	def isInside(self, otherWindow):
		return otherWindow.top > self.top and otherWindow.bottom < self.bottom and otherWindow.right < self.right and otherWindow.left > self.left
		
	def __str__(self):
		return "TL: ("+str(self.left)+","+str(self.top)+"); BR: ("+str(self.right)+","+str(self.bottom)+")"
		
	def setCount(self, count):
		self.count = count
		
	def addCount(self, addend):
		self.count += addend
		
